package com.zemential;/**
 * Created by TheL0w3R on 20/11/2018.
 * All Rights Reserved.
 */

import com.zemential.db.DBManager;
import javafx.application.Application;
import javafx.application.HostServices;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.*;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Objects;

public class MainApp extends Application {

    private static MainApp instance;

    public static void main(String[] args) {
        new DBManager();
        new GameManager();
        launch(args);
    }

    public static MainApp getInstance() {
        return instance;
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        instance = this;
        if(!new File("./Manual_de_Usuario.pdf").exists())
            Files.copy(getClass().getResourceAsStream("/doc/Manual_de_Usuario.pdf"), Paths.get("./Manual_de_Usuario.pdf"));
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/views/LoginStage.fxml")));
        primaryStage.setTitle("Zemential - Entrar");
        primaryStage.getIcons().add(new Image(getClass().getResource("/img/Logo_Zemential.png").toExternalForm()));
        primaryStage.setScene(new Scene(root));
        primaryStage.setResizable(false);
        primaryStage.show();
    }
}
