package com.zemential;

import com.zemential.types.GameSession;

/**
 * Created by TheL0w3R on 29/11/2018.
 * All Rights Reserved.
 */
public class GameManager {

    private static GameManager instance;

    private GameSession currentSession;

    public GameManager() {
        instance = this;
        currentSession = null;
    }

    public static GameManager getInstance() {
        return instance;
    }

    public GameSession startGameSession() {
        currentSession = new GameSession();
        return currentSession;
    }

    public GameSession getCurrentSession() {
        return currentSession;
    }
}
