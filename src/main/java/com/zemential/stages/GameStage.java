package com.zemential.stages;

import com.zemential.BaseController;
import com.zemential.GameManager;
import com.zemential.db.DBManager;
import com.zemential.types.GameResume;
import com.zemential.types.Option;
import com.zemential.types.Question;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.value.ObservableNumberValue;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * Created by TheL0w3R on 21/11/2018.
 * All Rights Reserved.
 */
public class GameStage extends BaseController {
    public Label questionLabel;
    public Label questionNumLabel;
    public ProgressBar timeLeftBar;
    public Button answer1Button;
    public Button answer2utton;
    public Button answer3Button;
    public Button answer4Button;
    public Label timeRemainingLabel;

    private Question current;
    private Thread timerThread;
    private Service timerService;

    public void initialize() {
        Platform.runLater(() -> {
            answer1Button.getScene().getWindow().addEventFilter(WindowEvent.WINDOW_CLOSE_REQUEST, this::closeWindowEvent);
        });

        timerService = new Service() {
            @Override
            protected Task createTask() {
                return new Task<Void>() {
                    final int N_ITERATIONS = 100;

                    @Override
                    protected Void call() throws Exception {
                        while(GameManager.getInstance().getCurrentSession().getCurrentQuestion() != null) {
                            for (int i = 0; i < N_ITERATIONS; i++) {
                                updateProgress(i + 1, N_ITERATIONS);
                                Thread.sleep(200);
                            }
                            Platform.runLater(GameStage.this::loadNextQuestion);
                        }
                        return null;
                    }
                };
            }
        };
        timeLeftBar.progressProperty().bind(
                timerService.progressProperty()
        );
        timeRemainingLabel.textProperty().bind(
                ReadOnlyStringProperty.stringExpression(Bindings.concat("Tiempo restante: ")).concat(
                        //ReadOnlyIntegerProperty.integerExpression(timerService.progressProperty().multiply(100).subtract(100).negate())
                        ReadOnlyIntegerProperty.integerExpression(timerService.progressProperty().multiply(20).subtract(100).negate().subtract(80))
                ).concat(" segundos")
        );
        answer1Button.setOnAction(this::triggerAnswer);
        answer2utton.setOnAction(this::triggerAnswer);
        answer3Button.setOnAction(this::triggerAnswer);
        answer4Button.setOnAction(this::triggerAnswer);
        loadNextQuestion();
    }

    private void closeWindowEvent(WindowEvent e) {
        timerService.cancel();
    }

    private void loadNextQuestion() {
        current = GameManager.getInstance().getCurrentSession().getNextQuestion();
        if(current == null) {
            timerService.cancel();
            GameResume resume = GameManager.getInstance().getCurrentSession().generateResume();
            showAlert(Alert.AlertType.NONE, "Has contestado " + (resume.getErrors() + resume.getHits()) +
                    "/10 preguntas.\nTu puntuación es: " +
                    resume.getErrors() + " errores y " + resume.getHits() +
                    " aciertos!", "Partida finalizada!", ButtonType.OK);
            DBManager.getInstance().insertGameResume(resume);
            ((Stage)questionLabel.getScene().getWindow()).close();
            return;
        }
        if(timerService.isRunning())
            timerService.cancel();
        timerService.reset();
        timerService.start();
        questionNumLabel.setText("PREGUNTA " + GameManager.getInstance().getCurrentSession().getLastIndex() + "/10");
        ArrayList<String> answers = new ArrayList<>(Arrays.asList(current.getAnswers()));
        Collections.shuffle(answers);
        if(current != null) {
            questionLabel.setText(current.getQuestion());
            answer1Button.setText(answers.get(0));
            answer2utton.setText(answers.get(1));
            answer3Button.setText(answers.get(2));
            answer4Button.setText(answers.get(3));
        }
    }

    private void triggerAnswer(ActionEvent event) {
        timerService.cancel();
        //boolean correct = current.answerQuestion(Option.valueOf(String.valueOf(((Button)event.getSource()).getText().charAt(0))).getValue());
        boolean correct = current.answerQuestion(Option.fromValue(current.getAnswerIndex(((Button)event.getSource()).getText())).getValue());
        if(correct)
            showAlert(Alert.AlertType.NONE, "Has contestado la opción correcta!", "Correcto!", ButtonType.OK);
        else
            showAlert(Alert.AlertType.NONE, "Has fallado!", "Oh no...", ButtonType.OK);
        loadNextQuestion();
    }
}
