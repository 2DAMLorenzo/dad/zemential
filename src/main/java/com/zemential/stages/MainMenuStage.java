package com.zemential.stages;

import com.zemential.BaseController;
import com.zemential.MainApp;
import com.zemential.db.DBManager;
import com.zemential.GameManager;
import com.zemential.types.GameSession;
import com.zemential.types.User;
import javafx.application.HostServices;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

/**
 * Created by TheL0w3R on 20/11/2018.
 * All Rights Reserved.
 */
public class MainMenuStage extends BaseController {
    public Button exitButton;
    public Button startGameButton;
    public Button historyButton;
    public Button manageQuestionsButton;
    public Label currentUserLabel;
    public Button helpButton;

    public void initialize() {
        User user = DBManager.getInstance().getLoggedUser();
        if(user != null)
            currentUserLabel.setText("Usuario actual: " + user.getName());
    }

    public void onExitButtonClick(ActionEvent actionEvent) {
        ((Stage)exitButton.getScene().getWindow()).close();
    }

    public void onHistoryButtonClick(ActionEvent actionEvent) {
        try {
            Stage primary = (Stage)exitButton.getScene().getWindow();
            showStage(primary, "HistoryStage", "Zemential - Historial");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onManageQuestionsButton(ActionEvent actionEvent) {
        try {
            showStage((Stage)exitButton.getScene().getWindow(), "QuestionManagerStage", "Zemential - Gestionar preguntas");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onStartGameButtonClick(ActionEvent actionEvent) {
        try {
            GameSession current = GameManager.getInstance().startGameSession();

            Stage primaryStage = (Stage)exitButton.getScene().getWindow();

            FXMLLoader loader = new FXMLLoader(Objects.requireNonNull(getClass().getResource("/views/GameStage.fxml")));
            Parent root = loader.load();
            GameStage controller = loader.getController();
            Stage stage = new Stage();
            stage.setTitle("Partida " + current.getDate().format(DateTimeFormatter.ofPattern("dd/M/yy")));
            stage.setScene(new Scene(root));
            stage.setOnHidden(e -> primaryStage.show());
            stage.getIcons().add(new Image(getClass().getResource("/img/Logo_Zemential.png").toExternalForm()));
            stage.show();

            primaryStage.hide();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onHelpButtonClick(ActionEvent actionEvent) {
        MainApp.getInstance().getHostServices().showDocument(new File("./Manual_de_Usuario.pdf").toURI().toString());
    }

    /*private void showStage(String name, String title) {
        Stage primaryStage = (Stage)exitButton.getScene().getWindow();

        try {
            Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("views/" + name + ".fxml")));
            Scene scene = new Scene(root);
            scene.getStylesheets().add(Objects.requireNonNull(getClass().getClassLoader().getResource("css/style.css")).toExternalForm());
            Stage stage = new Stage();
            stage.setTitle(title);
            stage.setScene(scene);

            stage.setOnHidden(e -> primaryStage.show());

            stage.show();

            primaryStage.hide();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/
}
