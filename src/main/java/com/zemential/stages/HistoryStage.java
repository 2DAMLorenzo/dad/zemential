package com.zemential.stages;

import com.zemential.BaseController;
import com.zemential.db.DBManager;
import com.zemential.types.GameResume;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

/**
 * Created by TheL0w3R on 20/11/2018.
 * All Rights Reserved.
 */
public class HistoryStage extends BaseController {

    public TableView<GameResume> historyTable;
    public TableColumn<GameResume, String> dateColumn;
    public TableColumn<GameResume, String> resultColumn;
    public Button clearHistoryButton;

    private ObservableList<GameResume> data = FXCollections.observableArrayList();

    public void initialize() {
        dateColumn.impl_setReorderable(false);
        dateColumn.prefWidthProperty().bind(historyTable.widthProperty().divide(4));
        dateColumn.setCellValueFactory(cellData -> new SimpleStringProperty(
                cellData.getValue().getMatchDate().format(DateTimeFormatter.ofPattern("dd/M/yy"))
        ));

        resultColumn.impl_setReorderable(false);
        resultColumn.prefWidthProperty().bind(historyTable.widthProperty().divide(4).multiply(3));
        resultColumn.setCellValueFactory(new PropertyValueFactory<>("formatted"));

        historyTable.setItems(data);
        updateTable();
    }

    public void onClearHistoryButton(ActionEvent actionEvent) {
        Optional res = showAlert(Alert.AlertType.WARNING, "¿Deseas continuar?", "Estás a punto de borrar el historial de partidas!", ButtonType.CANCEL, ButtonType.OK);
        if(res.isPresent() && res.get() == ButtonType.OK) {
            DBManager.getInstance().deleteGameResumes();
            updateTable();
        }
    }

    private void updateTable() {
        data.clear();
        data.addAll(DBManager.getInstance().getGameResumes());
    }
}
