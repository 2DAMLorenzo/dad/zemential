package com.zemential.stages;

import com.zemential.BaseController;
import com.zemential.db.DBManager;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.sqlite.SQLiteErrorCode;

/**
 * Created by TheL0w3R on 22/11/2018.
 * All Rights Reserved.
 */
public class RegisterStage extends BaseController {
    public TextField userField;
    public TextField passwordField;
    public Button registerButton;
    public Button backButton;

    public void onRegisterButtonClick(ActionEvent actionEvent) {
        if(userField.getText().isEmpty() && passwordField.getText().isEmpty()) {
            showAlert(Alert.AlertType.WARNING, "Para registrar un usuario tienes que rellenar todos los campos", "Hay campos vacíos!");
            return;
        }

        int result = DBManager.getInstance().registerUser(userField.getText(), passwordField.getText());
        if(result == SQLiteErrorCode.SQLITE_CONSTRAINT.code) {
            showAlert(Alert.AlertType.ERROR, "No puedes crear más de un usuario con el mismo nombre", "Ya existe un usuario con este nombre!");
            return;
        }

        showAlert(Alert.AlertType.INFORMATION, "Ya puedes iniciar sesión con el nuevo usuario", "Usuario creado con éxito!");
        ((Stage)backButton.getScene().getWindow()).close();
    }

    public void onBackButtonClick(ActionEvent actionEvent) {
        ((Stage)backButton.getScene().getWindow()).close();
    }
}
