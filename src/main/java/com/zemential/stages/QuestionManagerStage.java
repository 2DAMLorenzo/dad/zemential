package com.zemential.stages;

import com.zemential.BaseController;
import com.zemential.FileManager;
import com.zemential.db.DBManager;
import com.zemential.types.Option;
import com.zemential.types.Question;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

/**
 * Created by TheL0w3R on 21/11/2018.
 * All Rights Reserved.
 */
public class QuestionManagerStage extends BaseController {
    public TableView<Question> questionTable;
    public TableColumn<Question, String> idColumn;
    public TableColumn<Question, String> questionColumn;
    public TableColumn<Question, String> optionsColumn;
    public TableColumn<Question, Option> correctOptionColumn;
    public Button addButton;
    public Button deleteButton;
    public Label totalQuestionsLabel;
    public Button editQuestionButton;
    public MenuItem importMenuItem;
    public MenuItem exportMenuItem;
    public MenuItem deleteAllMenuItem;

    private ObservableList<Question> data = FXCollections.observableArrayList();

    public void initialize() {

        idColumn.impl_setReorderable(false);
        idColumn.prefWidthProperty().bind(questionTable.widthProperty().divide(9));
        idColumn.setCellValueFactory(new PropertyValueFactory<Question,String>("id"));

        questionColumn.impl_setReorderable(false);
        questionColumn.prefWidthProperty().bind(questionTable.widthProperty().divide(9).multiply(3));
        questionColumn.setCellValueFactory(new PropertyValueFactory<Question,String>("question"));

        optionsColumn.impl_setReorderable(false);
        optionsColumn.prefWidthProperty().bind(questionTable.widthProperty().divide(9).multiply(3));
        optionsColumn.setCellValueFactory(new PropertyValueFactory<Question,String>("answersAsString"));

        correctOptionColumn.impl_setReorderable(false);
        correctOptionColumn.prefWidthProperty().bind(questionTable.widthProperty().divide(9).multiply(2));
        correctOptionColumn.setCellValueFactory(new PropertyValueFactory<Question, Option>("correctIndex"));

        questionTable.setItems(data);
        updateData();
    }

    public void onAddButtonClick(ActionEvent actionEvent) {
        try {
            Stage stage = showModalDialog((Stage)addButton.getScene().getWindow(), "AddEditQuestionStage", "Añadir pregunta");
            stage.setOnHidden((WindowEvent e) -> {
                updateData();
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void onEditQuestionButtonClick(ActionEvent actionEvent) {
        int index = questionTable.getSelectionModel().getSelectedIndex();
        if(index == -1) {
            showAlert(Alert.AlertType.ERROR, "No hay ninguna pregunta seleccionada!", "Error al editar pregunta");
            return;
        }
        try {
            showEditQuestionStage("Editar pregunta", DBManager.getInstance().getQuestionByID(
                    questionTable.getSelectionModel().getSelectedItem().getId()
            ));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onDeleteButtonClick(ActionEvent actionEvent) {
        int index = questionTable.getSelectionModel().getSelectedIndex();
        if(index == -1) {
            showAlert(Alert.AlertType.ERROR, "No hay ninguna pregunta seleccionada!", "Error al eliminar pregunta");
            return;
        }

        Optional res = showAlert(Alert.AlertType.WARNING,
                "¿Estás seguro de que quierers eliminar la pregunta?",
                "Estás a punto de eliminar una pregunta!",
                ButtonType.CANCEL, ButtonType.OK
        );
        if(res.isPresent() && res.get() == ButtonType.OK) {
            DBManager.getInstance().deleteQuestion(questionTable.getSelectionModel().getSelectedItem().getId());
            updateData();
        }
    }

    private void updateData() {
        data.clear();
        data.addAll(Arrays.asList(DBManager.getInstance().fetchQuestions()));
        totalQuestionsLabel.setText("Preguntas totales: " + DBManager.getInstance().getAmountOfQuestions());
        System.out.println("UPDATING TABLE!");
    }

    private void showEditQuestionStage(String title, Question q) throws IOException {
        FXMLLoader loader = new FXMLLoader(Objects.requireNonNull(getClass().getResource("/views/AddEditQuestionStage.fxml")));
        Parent root = loader.load();
        AddEditQuestionStage controller = loader.getController();
        controller.enableEditMode(q);
        Scene scene = new Scene(root);
        scene.getStylesheets().add(Objects.requireNonNull(getClass().getResource("/css/style.css")).toExternalForm());
        Stage stage = new Stage();
        stage.setTitle(title);
        stage.getIcons().add(new Image(getClass().getResource("/img/Logo_Zemential.png").toExternalForm()));
        stage.setScene(scene);
        stage.setOnHidden((WindowEvent e) -> {
            updateData();
        });
        showModalDialog((Stage)editQuestionButton.getScene().getWindow(), stage);
    }

    public void onImportMenuItemClick(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Zemential question file", "*.zq"));
        File selectedFile = fileChooser.showOpenDialog(addButton.getScene().getWindow());
        if(selectedFile != null) {
            try {
                Question[] questions = FileManager.loadQuestions(selectedFile);

                Optional res = showAlert(Alert.AlertType.INFORMATION,
                        "¿Desea continuar?",
                        "El archivo contiene " + questions.length + " preguntas",
                        ButtonType.CANCEL, ButtonType.OK
                );

                if(res.isPresent() && res.get() == ButtonType.OK) {
                    DBManager.getInstance().insertQuestions(questions);
                    updateData();
                }
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public void onExportMenuItemClick(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Zemential question file", "*.zq"));
        File selectedFile = fileChooser.showSaveDialog(addButton.getScene().getWindow());
        if(selectedFile != null) {
            try {
                FileManager.saveQuestions(selectedFile, DBManager.getInstance().fetchQuestions());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void onDeleteAllMenuItemClick(ActionEvent actionEvent) {
        Optional res = showAlert(Alert.AlertType.INFORMATION,
                "¿Desea continuar?",
                "Está a punto de eliminar todas las preguntas!",
                ButtonType.CANCEL, ButtonType.OK
        );

        if(res.isPresent() && res.get() == ButtonType.OK) {
            DBManager.getInstance().deleteAllQuestions();
            updateData();
        }
    }
}
