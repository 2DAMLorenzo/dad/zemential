package com.zemential.stages;

import com.zemential.BaseController;
import com.zemential.db.DBManager;
import com.zemential.types.Option;
import com.zemential.types.Question;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.stage.Stage;

/**
 * Created by thel0w3r on 21/11/2018.
 * All Rights Reserved.
 */
public class AddEditQuestionStage extends BaseController {

    public TextField optionAField;
    public TextField optionBField;
    public TextField optionCField;
    public TextField optionDField;
    public ComboBox<Option> correctOptionCombo;
    public TextArea questionArea;
    public Button cancelButton;
    public Button saveButton;

    private boolean editMode = false;
    private Question currentQuestion = null;

    public void initialize() {
        correctOptionCombo.setItems(FXCollections.observableArrayList(Option.values()));
        correctOptionCombo.getSelectionModel().selectFirst();
    }

    public void enableEditMode(Question q) {
        editMode = true;
        currentQuestion = q;
        correctOptionCombo.getSelectionModel().select(q.getCorrectIndex().getValue());
        questionArea.setText(q.getQuestion());
        optionAField.setText(q.getAnswers()[0]);
        optionBField.setText(q.getAnswers()[1]);
        optionCField.setText(q.getAnswers()[2]);
        optionDField.setText(q.getAnswers()[3]);
    }

    public void onCancelButtonClick(ActionEvent actionEvent) {
        ((Stage)cancelButton.getScene().getWindow()).close();
    }

    public void onSaveButtonClick(ActionEvent actionEvent) {
        if(!questionArea.getText().isEmpty() &&
                !optionAField.getText().isEmpty() &&
                !optionBField.getText().isEmpty() &&
                !optionCField.getText().isEmpty() &&
                !optionDField.getText().isEmpty()) {
            Question finalQuestion = new Question((currentQuestion != null) ? currentQuestion.getId() : 0,
                    questionArea.getText(), new String[]{
                    optionAField.getText(),
                    optionBField.getText(),
                    optionCField.getText(),
                    optionDField.getText()
            }, correctOptionCombo.getSelectionModel().getSelectedItem().getValue());

            if(editMode)
                DBManager.getInstance().updateQuestion(finalQuestion);
            else
                DBManager.getInstance().insertQuestion(finalQuestion);
            ((Stage)cancelButton.getScene().getWindow()).close();
            return;
        }

        showAlert(Alert.AlertType.WARNING, "Para guardar tienes que rellenar todos los campos", "Hay campos vacíos!");
    }
}
