package com.zemential.stages;

import com.zemential.BaseController;
import com.zemential.db.DBManager;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by TheL0w3R on 22/11/2018.
 * All Rights Reserved.
 */
public class LoginStage extends BaseController {
    public Button loginButton;
    public Button registerButton;
    public TextField userField;
    public PasswordField passwordField;

    public void onLoginButtonClick(ActionEvent actionEvent) {
        boolean success = DBManager.getInstance().loginUser(userField.getText().trim(), passwordField.getText().trim());
        if(!success) {
            showAlert(Alert.AlertType.ERROR, "Usuario y/o contraseña inválidos", "Error al iniciar sesión!");
            return;
        }
        try {
            Stage stage = loadStage("MainMenuStage", "Zemential");
            stage.show();

            (loginButton.getScene().getWindow()).hide();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onRegisterButtonClick(ActionEvent actionEvent) {
        try {
            showStage((Stage)loginButton.getScene().getWindow(), "RegisterStage", "Zemential - Registro");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
