package com.zemential;

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.util.Objects;
import java.util.Optional;

/**
 * Created by TheL0w3R on 21/11/2018.
 * All Rights Reserved.
 */
public abstract class BaseController {

    protected Optional showAlert(Alert.AlertType type, String content, String header, ButtonType... buttons) {
        Alert alert = new Alert(type, content, buttons);
        alert.getDialogPane().getStylesheets().add(Objects.requireNonNull(getClass().getResource("/css/style.css")).toExternalForm());
        alert.setHeaderText(header);
        alert.initStyle(StageStyle.UTILITY);
        return alert.showAndWait();
    }

    protected Stage showModalDialog(Stage primaryStage, String name, String title) throws IOException {
        return showModalDialog(primaryStage, loadStage(name, title));
    }

    protected Stage showModalDialog(Stage primaryStage, Stage stage) {
        stage.initOwner(primaryStage);
        stage.initModality(Modality.APPLICATION_MODAL);

        Platform.runLater(stage::showAndWait);

        return stage;
    }

    protected void showStage(Stage primaryStage, String name, String title) throws IOException {
        Stage stage = loadStage(name, title);
        stage.setOnHidden(e -> primaryStage.show());
        stage.show();

        primaryStage.hide();
    }

    protected Stage loadStage(String name, String title) throws IOException {
        FXMLLoader loader = new FXMLLoader(Objects.requireNonNull(getClass().getResource("/views/" + name + ".fxml")));
        Parent root = loader.load();
        Stage stage = new Stage();
        stage.setTitle(title);
        stage.setScene(new Scene(root));
        stage.getIcons().add(new Image(getClass().getResource("/img/Logo_Zemential.png").toExternalForm()));
        return stage;
    }
}
