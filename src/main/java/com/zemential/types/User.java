package com.zemential.types;

/**
 * Created by TheL0w3R on 22/11/2018.
 * All Rights Reserved.
 */
public class User {

    private int id;
    private String name;

    public User(String name) {
        id = -1;
        this.name = name;
    }

    public User(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
