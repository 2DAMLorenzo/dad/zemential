package com.zemential.types;

/**
 * Created by TheL0w3R on 19/11/2018.
 * All Rights Reserved.
 */
public enum Option {

    A(0), B(1), C(2), D(3);

    private final int value;

    Option(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static Option fromValue(int value) {
        return values()[value];
    }
}
