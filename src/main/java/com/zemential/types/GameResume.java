package com.zemential.types;

import java.time.LocalDate;

/**
 * Created by thel0w3r on 2018-12-01.
 * All Rights Reserved.
 */
public class GameResume {

    private int id, errors, hits;
    private LocalDate matchDate;

    public GameResume(int id, int errors, int hits, LocalDate matchDate) {
        this.id = id;
        this.errors = errors;
        this.hits = hits;
        this.matchDate = matchDate;
    }

    public GameResume(int errors, int hits, LocalDate matchDate) {
        id = -1;
        this.errors = errors;
        this.hits = hits;
        this.matchDate = matchDate;
    }

    public int getId() {
        return id;
    }

    public int getErrors() {
        return errors;
    }

    public int getHits() {
        return hits;
    }

    public LocalDate getMatchDate() {
        return matchDate;
    }

    public String getFormatted() {
        return (errors + hits) + "/10 preguntas contestadas, " + hits + " aciertos y " + errors + " errores.";
    }
}
