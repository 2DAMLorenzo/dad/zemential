package com.zemential.types;

import com.zemential.db.DBManager;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;

/**
 * Created by TheL0w3R on 29/11/2018.
 * All Rights Reserved.
 */
public class GameSession {

    private ArrayList<Question> questions;
    private LocalDate date;
    private int lastIndex;

    public GameSession() {
        questions = new ArrayList<>();
        LinkedList<Question> copy = new LinkedList<>(Arrays.asList(DBManager.getInstance().fetchQuestions()));
        Collections.shuffle(copy);
        questions.addAll(copy.subList(0, 10));
        date = LocalDate.now();
        lastIndex = 0;
    }

    public LocalDate getDate() {
        return date;
    }

    public Question getNextQuestion() {
        if(questions.size() == lastIndex)
            return null;
        lastIndex++;
        return questions.get(lastIndex - 1);
    }

    public Question getCurrentQuestion() {
        return questions.get(lastIndex - 1);
    }

    public int getLastIndex() {
        return lastIndex;
    }

    public GameResume generateResume() {
        int numErrors = 0;
        int numHits = 0;
        for(Question q : questions) {
            if(q.isAnswered()) {
                if (q.isCorrect())
                    numHits++;
                else
                    numErrors++;
            }
        }
        return new GameResume(numErrors, numHits, date);
    }
}
