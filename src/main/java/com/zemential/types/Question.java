package com.zemential.types;

import java.io.Serializable;

/**
 * Created by TheL0w3R on 15/11/2018.
 * All Rights Reserved.
 */
public class Question implements Serializable {

    private int id;
    private String question;
    private String[] answers;
    private int correctIndex;

    private boolean answered;
    private boolean correct;

    public Question(String question, String[] answers, int correctIndex) {
        this(-1, question, answers, correctIndex);
    }

    public Question(int id, String question, String[] answers, int correctIndex) {
        this.id = id;
        this.question = question;
        this.answers = answers;
        this.correctIndex = correctIndex;
    }

    public int getId() {
        return id;
    }

    public String getQuestion() {
        return question;
    }

    public String[] getAnswers() {
        return answers;
    }

    public String getAnswersAsString() {
        return String.join(" · ", answers);
    }

    public Option getCorrectIndex() {
        return Option.fromValue(correctIndex);
    }

    public boolean answerQuestion(int selectedIndex) {
        answered = true;
        correct = selectedIndex == correctIndex;
        return correct;
    }

    public boolean isAnswered() {
        return answered;
    }

    public boolean isCorrect() {
        return correct;
    }

    public int getAnswerIndex(String answer) {
        for(int i = 0; i < answers.length; i++) {
            if(answers[i].equalsIgnoreCase(answer))
                return i;
        }
        return -1;
    }
}
