package com.zemential.db;

import com.zemential.types.Question;

import java.util.ArrayList;

/**
 * Created by thel0w3r on 20/11/2018.
 * All Rights Reserved.
 */
public class DBSeeder {

    private static ArrayList<Question> initialQuestions = new ArrayList<>();

    public static void seed() {
        initialQuestions.add(new Question(
                "En el código binario, ¿qué representan los dígitos?",
                new String[]{"Bytes", "Bits", "Registros de CPU","Unos y ceros"},
                1
        ));
        initialQuestions.add(new Question(
                "¿Cuál es el pico más elevado de España?",
                new String[]{"El Teide", "El Roque Nublo", "El Aneto", "El Mulhacén"},
                0
        ));
        initialQuestions.add(new Question(
                "De las siguientes patentes, ¿cuál no está relacionada con Nikola Tesla?",
                new String[]{"El generador electromagnético", "El generador eléctrico",
                        "La luz eléctrica", "La Bobina Tesla"},
                2
        ));
        initialQuestions.add(new Question(
                "¿Qué es la Alegoría de la Caverna de Platón?",
                new String[]{"Es una teoría que no enunció Platon, sino Aristóteles",
                        "Una paradoja literal sobre el devenir de los acontecimientos",
                        "Una metáfora con la que se explica como captar el mundo sensible y el mundo inteligible",
                        "El método que utiliza Platón para explicar el origen de todo cuanto conocemos"},
                2
        ));
        initialQuestions.add(new Question(
                "Con el resultado del Brexit aún por conocerse, ¿de cuántos países está compuesta la Unión Europea?",
                new String[]{"8", "20", "35", "28"},
                3
        ));
        initialQuestions.add(new Question(
                "Si una persona tiene heterocromía, ¿qué le ocurre?",
                new String[]{"Tiene el pelo de distintos colores de forma natural",
                        "Sufre de una percepción distinta de los colores, similar al daltonismo",
                        "Sus ojos son de distinto color",
                        "Es otro nombre para la psoriasis"},
                1
        ));
        initialQuestions.add(new Question(
                "¿Con qué figura literaria se identifica el estilo en el que habla Yoda, personaje de Star Wars?",
                new String[]{"Con la hipérbole", "Con el hipérbaton", "Con la anástrofe", "Con la metáfora"},
                2
        ));
        initialQuestions.add(new Question(
                "¿Qué ocurre si tenemos un dilema?",
                new String[]{"Una situación que, pudiéndose resolver de varias maneras, no se sabe cual elegir",
                        "Una deducción utilizando la lógica", "El rechazo a realizar una acción",
                        "Un problema imposible de resolver"},
                0
        ));
        initialQuestions.add(new Question(
                "El número 6'022 multiplicado por la vigésimotercera potencia de 10, ¿a qué corresponde?",
                new String[]{"A la masa del Sol", "Es el valor de un gúgol",
                        "No es ningún número en particular", "Al número de Avogadro"},
                3
        ));
        initialQuestions.add(new Question(
                "Si se traduce, de forma literal, la frase To be or not to be, ¿qué se obtiene en español?",
                new String[]{"Esa es la cuestión", "Vivir o morir",
                        "Cuando crees que me ves", "Ser o no ser"},
                3
        ));

        DBManager.getInstance().insertQuestions(initialQuestions.toArray(new Question[]{}));

        DBManager.getInstance().registerUser("admin", "admin");
    }


}
