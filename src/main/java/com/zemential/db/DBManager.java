package com.zemential.db;

import com.zemential.types.GameResume;
import com.zemential.types.Question;
import com.zemential.types.User;
import sun.util.resources.it.TimeZoneNames_it;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Created by TheL0w3R on 15/11/2018.
 * All Rights Reserved.
 */
public class DBManager {

    private static DBManager instance;

    private Connection conn;

    private User loggedUser;

    public DBManager() {
        instance = this;

        try {
            Class.forName("org.sqlite.JDBC");
            this.conn = DriverManager.getConnection("jdbc:sqlite:zemential.db");
            if(shouldCreateTables())
                initDatabase();

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public static DBManager getInstance() {
        return instance;
    }

    public User getLoggedUser() {
        return loggedUser;
    }

    private boolean shouldCreateTables(){
        try{
            Statement statement = conn.createStatement();
            statement.setQueryTimeout(30);
            ResultSet rs = statement.executeQuery("SELECT count(*) FROM sqlite_master WHERE (type='table' AND name='questions') OR (type='table' AND name='history')");
            if(rs.next()) {
                if(rs.getInt(1) < 2)
                    return true;
            }
        }catch(SQLException ex){
            ex.printStackTrace();
        }
        return false;
    }

    private void initDatabase() {
        try {
            Statement statement = conn.createStatement();
            statement.setQueryTimeout(30);
            statement.executeUpdate(DBQuery.CREATE_QUESTIONS_TABLE);
            statement.executeUpdate(DBQuery.CREATE_HISTORY_TABLE);
            statement.executeUpdate(DBQuery.CREATE_USERS_TABLE);
            DBSeeder.seed();
            System.out.println("Database initialized!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int registerUser(String username, String password) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.reset();
            md.update(password.getBytes());
            PreparedStatement statement = conn.prepareStatement(DBQuery.INSERT_USER);
            statement.setQueryTimeout(30);
            statement.setString(1, username);
            statement.setString(2, new BigInteger(1, md.digest()).toString(16));
            return statement.executeUpdate();
        } catch (SQLException sqlite) {
            return sqlite.getErrorCode();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return 1;
    }

    public boolean loginUser(String username, String password) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.reset();
            md.update(password.getBytes());
            String pw = new BigInteger(1, md.digest()).toString(16);
            PreparedStatement statement = conn.prepareStatement(DBQuery.LOGIN_USER);
            statement.setQueryTimeout(30);
            statement.setString(1, username);
            statement.setString(2, pw);
            ResultSet rs = statement.executeQuery();
            if(rs.next()) {
                this.loggedUser = new User(rs.getInt("id"), rs.getString("username"));
                return true;
            }
        } catch (NoSuchAlgorithmException | SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public Question getQuestionByID(int id) {
        Question question = null;
        try {
            Statement statement = conn.createStatement();
            statement.setQueryTimeout(30);
            ResultSet rs = statement.executeQuery("SELECT * FROM questions WHERE id = " + id);
            while(rs.next()) {
                question = new Question(
                        rs.getInt("id"),
                        rs.getString("question"),
                        rs.getString("answers").split(" · "),
                        rs.getInt("correctAnswer")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return question;
    }

    public int getAmountOfQuestions() {
        int amount = 0;
        try {
            Statement statement = conn.createStatement();
            statement.setQueryTimeout(30);
            ResultSet rs = statement.executeQuery("SELECT COUNT(id) FROM questions");
            while(rs.next()) {
                amount = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return amount;
    }

    public Question[] fetchQuestions() {
        ArrayList<Question> questions = new ArrayList<>();
        try {
            Statement statement = conn.createStatement();
            statement.setQueryTimeout(30);
            ResultSet rs = statement.executeQuery("SELECT * FROM questions");
            while(rs.next()) {
                questions.add(
                        new Question(
                                rs.getInt("id"),
                                rs.getString("question"),
                                rs.getString("answers").split(" · "),
                                rs.getInt("correctAnswer")
                        )
                );

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return questions.toArray(new Question[]{});
    }

    public void deleteQuestion(int id) {
        try {
            PreparedStatement statement = conn.prepareStatement(DBQuery.DELETE_BY_ID.replace("$table", "questions"));
            statement.setQueryTimeout(30);
            statement.setInt(1, id);
            int res = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAllQuestions() {
        try {
            PreparedStatement statement = conn.prepareStatement("DELETE FROM questions");
            statement.setQueryTimeout(30);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertQuestion(Question q) {
        try {
            PreparedStatement statement = conn.prepareStatement(DBQuery.INSERT_QUESTION);
            statement.setQueryTimeout(30);
            statement.setString(1, q.getQuestion());
            statement.setString(2, q.getAnswersAsString());
            statement.setInt(3, q.getCorrectIndex().getValue());
            int res = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertQuestions(Question[] questions) {
        try {
            conn.setAutoCommit(false);
            for(Question question : questions) {
                insertQuestion(question);
            }
            conn.commit();
            conn.setAutoCommit(true);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateQuestion(Question q) {
        try {
            PreparedStatement statement = conn.prepareStatement(DBQuery.UPDATE_QUESTION);
            statement.setQueryTimeout(30);
            statement.setString(1, q.getQuestion());
            statement.setString(2, q.getAnswersAsString());
            statement.setInt(3, q.getCorrectIndex().getValue());
            statement.setInt(4, q.getId());
            int res = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertGameResume(GameResume gr) {
        try {
            PreparedStatement statement = conn.prepareStatement(DBQuery.INSERT_HISTORY);
            statement.setQueryTimeout(30);
            statement.setString(1, gr.getMatchDate().toString());
            statement.setInt(2, gr.getErrors());
            statement.setInt(3, gr.getHits());
            int res = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public GameResume[] getGameResumes() {
        ArrayList<GameResume> resumes = new ArrayList<>();
        try {
            Statement statement = conn.createStatement();
            statement.setQueryTimeout(30);
            ResultSet rs = statement.executeQuery("SELECT * FROM history ORDER BY id DESC");
            while(rs.next()) {
                resumes.add(
                        new GameResume(
                                rs.getInt("id"),
                                rs.getInt("errors"),
                                rs.getInt("hits"),
                                LocalDate.parse(rs.getString("matchDate"))
                        )
                );

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resumes.toArray(new GameResume[]{});
    }

    public void deleteGameResumes() {
        try {
            PreparedStatement statement = conn.prepareStatement("DELETE FROM history");
            statement.setQueryTimeout(30);
            int res = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
