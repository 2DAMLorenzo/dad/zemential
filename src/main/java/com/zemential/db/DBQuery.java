package com.zemential.db;

/**
 * Created by TheL0w3R on 19/11/2018.
 * All Rights Reserved.
 */
public class DBQuery {

    public static final String CREATE_QUESTIONS_TABLE = "CREATE TABLE IF NOT EXISTS questions (id INTEGER PRIMARY KEY AUTOINCREMENT , question STRING, answers STRING, correctAnswer INTEGER)";
    public static final String CREATE_HISTORY_TABLE = "CREATE TABLE IF NOT EXISTS history (id INTEGER PRIMARY KEY AUTOINCREMENT, matchDate STRING, errors INTEGER, hits INTEGER)";
    public static final String CREATE_USERS_TABLE = "CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY AUTOINCREMENT, username STRING, password STRING, UNIQUE(username))";

    public static final String DELETE_BY_ID = "DELETE FROM $table where id = ?";

    public static final String INSERT_QUESTION = "INSERT INTO questions(question, answers, correctAnswer) VALUES(?, ?, ?)";
    public static final String INSERT_HISTORY = "INSERT INTO history(matchDate, errors, hits) VALUES(?, ?, ?)";
    public static final String UPDATE_QUESTION = "UPDATE questions SET question = ?, answers = ?, correctAnswer = ? WHERE id = ?";

    public static final String INSERT_USER = "INSERT INTO users(username, password) VALUES(?, ?)";
    public static final String LOGIN_USER = "SELECT * FROM users WHERE (username=? AND password=?)";

}
