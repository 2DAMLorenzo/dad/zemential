package com.zemential;

import com.zemential.types.Question;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by TheL0w3R on 28/11/2018.
 * All Rights Reserved.
 */
public class FileManager {

    public static void saveQuestions(File out, Question[] questions) throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(out));

        for(Question q : questions)
            oos.writeObject(q);

        oos.flush();
        oos.close();
    }

    public static Question[] loadQuestions(File in) throws IOException, ClassNotFoundException {
        ArrayList<Question> questions = new ArrayList<>();

        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(in));
        try {
            while(true) {
                questions.add((Question) ois.readObject());
            }
        } catch(EOFException ignored){}

        return questions.toArray(new Question[]{});
    }

}
