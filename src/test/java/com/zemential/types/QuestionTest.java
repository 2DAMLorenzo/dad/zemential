package com.zemential.types;

import com.zemential.types.Question;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by thel0w3r on 19/11/2018.
 * All Rights Reserved.
 */
public class QuestionTest {

    Question q = new Question(1, "Test", new String[]{"answer1", "answer2", "answer3", "answer4"}, 0);

    @Test
    public void testGetRawReturnsCorrectValues() {
        //assertArrayEquals(q.getRaw(), new String[]{"1", "Test", "answer1 · answer2 · answer3 · answer4", "A"});
    }

    @Test
    public void testGetAnswersAsStringReturnsCorrectValue() {
        //assertEquals(q.getAnswersAsString(), "answer1<>answer2<>answer3<>answer4");
    }
}