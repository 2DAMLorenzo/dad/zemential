package com.zemential.types;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by TheL0w3R on 19/11/2018.
 * All Rights Reserved.
 */
public class OptionsTest {

    @Test
    public void testGetValueReturnsCorrectValue() {
        assertEquals(Option.A.getValue(), 0);
    }

    @Test
    public void testToStringReturnsCorrectValue() {
        assertEquals(Option.A.toString(), "A");
    }

}